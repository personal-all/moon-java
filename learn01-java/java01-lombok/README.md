### Lombok 示例

1. 在 IDEA 中要使用 lombok 需要安装 lombok 插件；
2. IDEA 插件不能和 AspectJ 编译插件同时使用，暂时也没有合适的解决方法；
> AspectJ 静态代理我们目前几乎不用，都是用 Cglib 动态代理  