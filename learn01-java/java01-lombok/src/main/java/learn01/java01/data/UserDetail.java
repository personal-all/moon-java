package learn01.java01.data;

import lombok.*;

/**
 * {@link Data} 注解默认生成 getter、setter、toString、hashCode、equals 等方法
 * 但不会调用父类 equals 等方法
 *
 * @author benshaoye
 */
@Data
// 无参构造器，由于有 final 字段，不能指定 无参构造器
// @NoArgsConstructor
// @Data 默认会生成 必须参数构造器，所以 @RequiredArgsConstructor 可以不指定
@RequiredArgsConstructor
@AllArgsConstructor
public class UserDetail {

    private final String name;

    private int age;

    private String sex;
    /**
     * 指定访问级别
     */
    @Getter(value = AccessLevel.PROTECTED)
    @Setter(value = AccessLevel.PRIVATE)
    private int score;

    /**
     * 排除 equals hashCode 等
     */
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private double money;



    @Data
    public static class UserInformation{
        private String name;

        private int age;

        private String sex;
    }
}
