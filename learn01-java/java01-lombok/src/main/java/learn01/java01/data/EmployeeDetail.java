package learn01.java01.data;

import lombok.*;

/**
 * @author benshaoye
 */
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class EmployeeDetail extends UserDetail {

    private String address;

    public EmployeeDetail(String name) {
        super(name);
    }

}
