package learn01.java01.io;

import lombok.Cleanup;

/**
 * {@link Cleanup}会自动关闭流
 * 相当于 try{} catch {} finally {}
 *
 * @author benshaoye
 */
public class CloseableInputStream implements AutoCloseable {

    public CloseableInputStream() {

    }

    @Override
    public void close() throws Exception {
        @Cleanup final AutoCloseableOutputStream outputoutput = new AutoCloseableOutputStream();
        System.out.println("自动关闭完成" + getClass());
    }

    public static class AutoCloseableOutputStream implements AutoCloseable {

        @Override
        public void close() throws Exception {
            System.out.println("自动关闭完成" + getClass());
        }
    }
}
