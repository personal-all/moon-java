package learn01.java01.exception;

import lombok.SneakyThrows;

/**
 * @author benshaoye
 */
public class ExceptionMain {

    public static void handle01Method() throws Exception {
        throw new Exception();
    }

    /**
     * 这个注解{@link SneakyThrows}会在方法内部将会抛出异常的方法用 try {} catch {} 包裹
     */
    @SneakyThrows
    public static void handle02Method(){
        handle01Method();
    }

    public static void main(String[] args) {
        handle02Method();
    }

    public ExceptionMain() {
    }
}
