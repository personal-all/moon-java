package learn01.java01.builder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * lombok 建造者模式
 *
 * @author benshaoye
 */
@Builder
@AllArgsConstructor(staticName = "of")
@Data(staticConstructor = "of")
public class MemberEntity {

    private String name;

    private String id;

    private String idcard;

    private String address;

    public static void main(String[] args) {
        MemberEntity member = MemberEntity.builder()
            .idcard("10012645785422")
            .name("张三")
            .id("123456")
            .address("北京市西直门大街")
            .build();
        System.out.println(member);
    }
}
