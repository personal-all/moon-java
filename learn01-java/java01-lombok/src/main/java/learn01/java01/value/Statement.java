package learn01.java01.value;

import lombok.val;
import lombok.var;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * 用于声明局部变量，二者的区别就是有没有 final
 * {@link lombok.val}
 * {@link lombok.var} 参考 java11 中的 var 关键字
 *
 * @author benshaoye
 */
public class Statement {

    public Statement() {
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        // 相当于 final List list = new Arraylist<>();
        val list = new ArrayList<>();
        // 相当于 List list = new Arraylist<>();
        var set = new HashSet<>();
    }
}
