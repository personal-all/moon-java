package learn01.java01.with;

import lombok.Data;
import lombok.With;

/**
 * With 暂时不支持，应该是 插件问题，用的也不多，暂时不管了
 *
 * @author benshaoye
 */
@Data(staticConstructor = "of")
public class WithDemo {

    private int value;

    public WithDemo() {
    }

    public static void main(String[] args) {
        // WithDemo.of().withValue()
    }
}
