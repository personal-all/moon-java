package learn01.java01.exception;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author benshaoye
 */
class ExceptionMainTestTest {

    @Test
    void testHandle02Method() throws Exception {
        assertThrows(Exception.class,() -> {
            ExceptionMain.handle02Method();
        });
    }

    @Test
    void testHandle01Method() throws Exception {
        assertThrows(Exception.class,() -> {
            ExceptionMain.handle01Method();
        });

    }
}