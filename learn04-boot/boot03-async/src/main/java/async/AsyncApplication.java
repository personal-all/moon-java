package async;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;


/**
 * 关键：
 * 1. 开启注解：{@link EnableAsync}
 * 2. 在任务上注解：{@link Async}
 *
 * @author benshaoye
 * @see Tasks 异步任务需要主动触发{@link RequestController#execute()}
 */
@EnableAsync
@SpringBootApplication
public class AsyncApplication {

    public static void main(String[] args) {
        SpringApplication.run(AsyncApplication.class, args);
    }
}
