package async;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author benshaoye
 */
@RestController
public class RequestController {

    @Autowired
    private Tasks tasks;

    @GetMapping("execute")
    public void execute() {
        tasks.task1();
        tasks.task2();
    }
}
