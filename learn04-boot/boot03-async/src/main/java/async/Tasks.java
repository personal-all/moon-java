package async;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @author benshaoye
 */
@Slf4j
@Component
public class Tasks {

    public Tasks() {}

    @Async("taskExecutor1")
    public void task1() {
        log.info("Task 001 开始");
        try{
            Thread.sleep(5000);
        }catch (Exception e){
            e.printStackTrace();
        }
        log.info("Task 001 结束");
    }

    @Async("taskExecutor2")
    public void task2() {
        log.info("Task 002 开始");
        try{
            Thread.sleep(5000);
        }catch (Exception e){
            e.printStackTrace();
        }
        log.info("Task 002 结束");
    }
}
