package hello.config;

import lombok.Data;

/**
 * @author benshaoye
 */
@Data
public class ChildrenProperties {

    private String host;

    private int port;

    public ChildrenProperties() {
    }
}
