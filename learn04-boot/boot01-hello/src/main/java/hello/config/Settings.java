package hello.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 属性必须有 setter 方法才能取值
 *
 * @author benshaoye
 */
@ConfigurationProperties("app.settings")
public class Settings {

    private String name;
    private int value;

    public Settings() {
    }
}
