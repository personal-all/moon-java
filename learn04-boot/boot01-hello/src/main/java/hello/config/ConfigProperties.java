package hello.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 属性必须有 setter 方法才能取值：{@link Settings}
 * <p>
 * <p>
 * 配置信息设置方式:
 *
 * @author benshaoye
 * @see Settings 参考 这个方式，两个注解位置不需在一起
 */
@Data
// 如果需要使用这种方式设置配置信息，那么对应的信息类 ConfigProperties 必须
// 注册到 EnableConfigurationProperties 中
@ConfigurationProperties("app.config")
public class ConfigProperties {

    private String name;

    private int value;

    private final ChildrenProperties children = new ChildrenProperties();

    public ConfigProperties() {
    }
}
