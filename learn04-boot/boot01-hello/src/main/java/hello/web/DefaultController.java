package hello.web;

import hello.config.ConfigProperties;
import hello.config.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * @author benshaoye
 */
@RestController
public class DefaultController {

    @Value("${app.name}")
    private String appName;
    @Value("${app.host:null}")
    private String appHost;

    @Autowired
    private Settings settings;

    @Autowired
    private ConfigProperties properties;

    @GetMapping("hello")
    public Object hello() {
        return "Hello World";
    }

    @GetMapping("defaults")
    public Object defaults() {
        return new HashMap() {{
            put("appName", appName);
            put("appHost", appHost);
        }};
    }

    @GetMapping("settings")
    public Object settings() {
        return settings;
    }

    @GetMapping("properties")
    public Object properties() {
        return properties;
    }
}
