package hello;

import hello.config.ConfigProperties;
import hello.config.Settings;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * @author benshaoye
 */
@EnableConfigurationProperties({Settings.class, ConfigProperties.class})
@SpringBootApplication
public class HelloApplication {

    public static void main(String[] args) {
        SpringApplication.run(HelloApplication.class);
    }
}
