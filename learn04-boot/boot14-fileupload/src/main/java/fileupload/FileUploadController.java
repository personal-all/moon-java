package fileupload;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author benshaoye
 */
@Slf4j
@Controller
public class FileUploadController {

    @PostMapping("upload")
    public String upload(MultipartFile file) {
        return "index.html";
    }
}
