package cache;

import lombok.Data;

/**
 * @author benshaoye
 */
@Data
public class UserModel {

    private String id;
    private String name;
    private int age;

    public UserModel() {
    }
}
