package cache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author benshaoye
 */
@Slf4j
@RestController
public class CacheController implements InitializingBean {

    @Autowired
    private DataBaseProxy database;

    @CachePut(value = "get-user", key = "#id")
    @GetMapping("get")
    public UserModel get(String id) {
        log.info("第一次获取 user id：{}", id);
        return Arrays.stream(database.getList()).filter(user -> Objects.equals(id, user.getId())).findFirst().get();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("=================================================================");
        log.info(database.toString());
        log.info("=================================================================");
    }
}
