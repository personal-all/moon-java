package cache;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author benshaoye
 */
@Data
@ConfigurationProperties("data")
public class DataBaseProxy {

    private UserModel[] list;

    public DataBaseProxy() {
    }
}
