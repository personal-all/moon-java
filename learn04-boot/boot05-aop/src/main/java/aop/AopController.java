package aop;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author benshaoye
 */
@Slf4j
@RestController
public class AopController {

    @GetMapping("insert")
    @VerifyIdempotency
    public void insert() {
        log.info("insert in controller");
    }

    @GetMapping("update")
    @VerifyIdempotency
    public void update() {
        log.info("update in controller");
    }
}
