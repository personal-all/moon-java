package aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * # spring aop 模块：
 * <p>
 * 相比 boot01 ~ boot04, boot05-aop 需要依赖
 * <pre>
 *     <dependency>
 *         <groupId>aopalliance</groupId>
 *         <artifactId>aopalliance</artifactId>
 *         <version>1.0</version>
 *     </dependency>
 *     <dependency>
 *         <groupId>org.springframework.boot</groupId>
 *         <artifactId>spring-boot-starter-aop</artifactId>
 *     </dependency>
 * </pre>
 * <p>
 * <p>
 * ## 开启方式：
 * 1. 创建切面类，如：{@link Aop}
 * 2. 在切面类上应用注解：{@link Component}表示这受{@code spring}管理和{@link Aspect}表示这是个切面类
 * 3. 编写切面代码，如：{@link Aop#after(JoinPoint)}
 * <p>
 * ## 相关注解
 * {@link Before}: 执行前
 * {@link Around}: 环绕执行
 * {@link After}: 执行后
 * {@link AfterThrowing}: 抛出异常后
 * {@link AfterReturning}: 返回后
 * <p>
 * ## 此外还应关注：{@link Order}和{@link Ordered}当有多个处理器用于同一个切面时，可指定顺序，否则顺序不可控
 *
 * @author benshaoye
 */
// @EnableAspectJAutoProxy
@SpringBootApplication
public class AopApplication {

    public static void main(String[] args) {
        SpringApplication.run(AopApplication.class, args);
    }
}
