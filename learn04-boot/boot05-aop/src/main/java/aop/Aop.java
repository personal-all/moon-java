package aop;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * @author benshaoye
 */
@Slf4j
@Aspect
@Component
public class Aop {

    @Pointcut("execution(@aop.VerifyIdempotency public * *(..))")
    public void pointCut() {}

    @Before("pointCut()")
    public void before(JoinPoint point) {
        // log.info("before");
        System.out.println("before");
    }


    @SneakyThrows
    @Around("pointCut()")
    public void around(ProceedingJoinPoint point) {
        System.out.println("around before");
        point.proceed();
        System.out.println("around after");
    }


    @After("pointCut()")
    public void after(JoinPoint point) {
        System.out.println("after");
    }
}
