package interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author benshaoye
 */
@Configuration
public class ChainInterceptors {

    public ChainInterceptors() {
    }

    @Slf4j
    @Component
    public static class OneChainInterceptor implements HandlerInterceptor {

        @Override
        public boolean preHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler
        ) throws Exception {
            log.info("URL: {}, Target: {}, On: {}", request.getRequestURI(), getClass().getSimpleName(), "preHandle");
            return true;
        }

        @Override
        public void postHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView
        ) throws Exception {
            log.info("URL: {}, Target: {}, On: {}", request.getRequestURI(), getClass().getSimpleName(), "postHandle");
        }

        @Override
        public void afterCompletion(
            HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex
        ) throws Exception {
            log.info("URL: {}, Target: {}, On: {}", request.getRequestURI(), getClass().getSimpleName(), "afterCompletion");
        }
    }

    @Slf4j
    @Component
    public static class TwoChainInterceptor implements HandlerInterceptor {
        @Override
        public boolean preHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler
        ) throws Exception {
            log.info("URL: {}, Target: {}, On: {}", request.getRequestURI(), getClass().getSimpleName(), "preHandle");
            return true;
        }

        @Override
        public void postHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView
        ) throws Exception {
            log.info("URL: {}, Target: {}, On: {}", request.getRequestURI(), getClass().getSimpleName(), "postHandle");
        }

        @Override
        public void afterCompletion(
            HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex
        ) throws Exception {
            log.info("URL: {}, Target: {}, On: {}", request.getRequestURI(), getClass().getSimpleName(), "afterCompletion");
        }
    }
}
