package interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author benshaoye
 */
@Slf4j
@RestController
public class ChainController {

    public ChainController() {
    }

    @GetMapping("chain/interceptor")
    public Object chain(HttpServletRequest request) {
        log.info("{} ===> controller", request.getRequestURI());
        return request.getRequestURI();
    }
}
