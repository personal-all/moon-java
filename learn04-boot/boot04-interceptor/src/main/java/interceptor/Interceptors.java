package interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author benshaoye
 */
public class Interceptors {

    public Interceptors() {}

    @Slf4j
    @Component
    public static class FirstInterceptor implements HandlerInterceptor {

        @Override
        public boolean preHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler
        ) throws Exception {
            log.info("================================");
            log.info("[{}] preHandle: 这里必须返回 true 才会进入 controller 执行", request.getRequestURI());
            HandlerMethod method = (HandlerMethod) handler;
            return true;
        }

        @Override
        public void postHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView
        ) throws Exception {
            log.info("[{}] postHandle: controller 正常执行完后才执行这个", request.getRequestURI());
        }

        @Override
        public void afterCompletion(
            HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex
        ) throws Exception {
            log.info("[{}] afterCompletion: 这个始终会被执行, ex: [{}]", request.getRequestURI(), String.valueOf(ex));
        }
    }

    @Slf4j
    @Component
    public static class SecondInterceptor implements HandlerInterceptor {

        @Override
        public boolean preHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler
        ) throws Exception {
            log.info("================================");
            log.info("[{}] preHandle: 这里必须返回 false，所以不会进入 controller", request.getRequestURI());
            return false;
        }

        @Override
        public void postHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView
        ) throws Exception {
            log.info("[{}] postHandle: controller 正常执行完后才执行这个", request.getRequestURI());
        }

        @Override
        public void afterCompletion(
            HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex
        ) throws Exception {
            log.info("[{}] afterCompletion: 这个始终会被执行, ex: [{}]", request.getRequestURI(), String.valueOf(ex));
        }
    }
}
