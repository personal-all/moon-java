package interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author benshaoye
 */
@Slf4j
@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {

    @Autowired
    private Interceptors.FirstInterceptor firstInterceptor;
    @Autowired
    private Interceptors.SecondInterceptor secondInterceptor;
    @Autowired
    private ChainInterceptors.OneChainInterceptor oneChainInterceptor;
    @Autowired
    private ChainInterceptors.TwoChainInterceptor twoChainInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(firstInterceptor).addPathPatterns("/first/**");
        registry.addInterceptor(secondInterceptor).addPathPatterns("/second/**");

        registry.addInterceptor(oneChainInterceptor).addPathPatterns("/chain/**").order(0);
        registry.addInterceptor(twoChainInterceptor).addPathPatterns("/chain/**").order(1);
    }
}
