package interceptor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 注册拦截器通过实现接口{@link WebMvcConfigurer#addInterceptors(InterceptorRegistry)}实现
 * <p>
 * 1. 添加拦截器，设置拦截 url 规则
 * <p>
 * 2. 拦截器必须实现接口{@link HandlerInterceptor}
 * 3. {@link HandlerInterceptor#preHandle(HttpServletRequest, HttpServletResponse, Object)}必须返回 true 才会执行后面的操作
 * 4. {@link HandlerInterceptor#postHandle(HttpServletRequest, HttpServletResponse, Object, ModelAndView)}是在 controller
 * 正常支出完后才会被执行
 * <p>
 * 5. {@link HandlerInterceptor#afterCompletion(HttpServletRequest, HttpServletResponse, Object, Exception)}无论
 * controller 是正常执行完成或者抛出异常，均会被执行
 * <p>
 * 6. 同一个 controller 处理方法多个拦截器的执行顺序参考{@link ChainInterceptors}、{@link ChainController#chain(HttpServletRequest)}
 *
 * @author benshaoye
 */
@SpringBootApplication
public class InterceptorApplication {

    public static void main(String[] args) {
        SpringApplication.run(InterceptorApplication.class, args);
    }
}
