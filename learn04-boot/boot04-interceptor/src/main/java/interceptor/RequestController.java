package interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author benshaoye
 */
@Slf4j
@RestController
public class RequestController {

    @GetMapping("first/default")
    public Object firstDefault(HttpServletRequest request) {
        log.info("[{}] controller >>>>>>", request.getRequestURI());
        return "firstDefault";
    }

    @GetMapping("first/throw")
    public Object firstThrow(HttpServletRequest request) {
        log.info("[{}] controller >>>>>>", request.getRequestURI());
        throw new IllegalStateException();
    }

    @GetMapping("second/default")
    public Object secondDefault(HttpServletRequest request) {
        log.info("[{}] controller >>>>>>", request.getRequestURI());
        return "secondDefault";
    }
}
