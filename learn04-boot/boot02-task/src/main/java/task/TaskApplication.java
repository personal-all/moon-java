package task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * 关键：
 * 1. 开启注解：{@link EnableScheduling}
 * 2. 在任务上注解：{@link Scheduled}
 *
 * @author benshaoye
 * @see Tasks 定时任务不需要主动触发
 */
@EnableScheduling
@SpringBootApplication
public class TaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaskApplication.class, args);
    }
}
