package task;

import com.moon.core.util.CPUUtil;
import com.moon.core.util.concurrent.ExecutorUtil;
import com.moon.core.util.concurrent.RejectedUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author benshaoye
 */
@Configuration
public class Executors {

    public Executors() {
    }

    @Bean("taskExecutor")
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        // 允许核心线程数超时：所有超时的线程均会被关闭
        executor.setAllowCoreThreadTimeOut(true);
        // 核心线程数
        executor.setCorePoolSize(CPUUtil.getCoreCount());
        // 最大线程数
        executor.setMaxPoolSize(CPUUtil.getCoreCount() * 2);
        // 线程名前缀：方便定位
        executor.setThreadNamePrefix("Executor01-");
        // 设置任务队列容量
        executor.setQueueCapacity(1024);
        // 允许线程的空闲时间60秒：当超过了核心线程出之外的线程在空闲时间到达之后会被销毁
        // 如果设置了允许核心线程超时，那么核心线程超时也会被销毁
        executor.setKeepAliveSeconds(60);
        // 拒绝策略：当不能接受新任务时，有提交任务的线程执行任务
        executor.setRejectedExecutionHandler(RejectedUtil.callerRun());
        // 当线程池关闭时等待其他所有任务完成
        executor.setWaitForTasksToCompleteOnShutdown(true);
        // 设置线程池中任务的等待时间，如果超过这个时候还没有销毁就强制销毁，以确保应用最后能够被关闭，而不是阻塞住。
        executor.setAwaitTerminationSeconds(600);
        return executor;
    }
}
