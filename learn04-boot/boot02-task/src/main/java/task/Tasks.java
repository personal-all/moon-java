package task;

import com.moon.core.util.DateUtil;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author benshaoye
 */
@Component
public class Tasks {

    public Tasks() {
    }

    /**
     * 第二次任务总是在第一次任务结束后 5 秒开始
     *
     * @see Scheduled#fixedDelay() 指定两次任务之间时间间隔
     */
    @Scheduled(fixedDelay = 5000)
    public void fixedDelay() {
        System.out.println(getClass() + " fixedDelay\t" + DateUtil.format());
    }

    /**
     * 两次任务起始时间的时间间隔是 5 秒
     */
    @Scheduled(fixedRate = 4000)
    public void fixedRate() {
        System.out.println(getClass() + " fixedRate\t" + DateUtil.format());
    }

    /**
     * 第一次执行时间推迟 1.2 秒
     */
    @Scheduled(fixedRate = 3000, initialDelay = 1200)
    public void deferredExecute() {
        System.out.println(getClass() + " deferredExecute\t" + DateUtil.format());
    }


    public void cron(){

    }
}
