package provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author benshaoye
 */
@SpringBootApplication
public class HystrixProviderApplication6821 {

    public static void main(String[] args) {
        SpringApplication.run(HystrixProviderApplication6821.class, args);
    }
}
