package provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author benshaoye
 */
@SpringBootApplication
public class HystrixProviderApplication6820 {

    public static void main(String[] args) {
        SpringApplication.run(HystrixProviderApplication6820.class, args);
    }
}
