package provider;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author benshaoye
 */
@RestController
public class ProviderController {

    @GetMapping("getMaleInfo")
    public Map getMaleInfo() {
        return new HashMap(4) {{
            put("name", "本少爷");
            put("age", 25);
            put("sex", "男");
        }};
    }

    @GetMapping("getFemaleInfo")
    public Map getFemaleInfo() {
        return new HashMap(4) {{
            put("name", "小仙女");
            put("age", 22);
            put("sex", "女");
        }};
    }

    @GetMapping("getThrowInfo")
    public Map getThrowInfo() {
        throw new IllegalStateException("未知错误");
    }
}
