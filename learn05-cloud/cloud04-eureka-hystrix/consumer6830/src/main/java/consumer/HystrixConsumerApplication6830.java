package consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author benshaoye
 */
@EnableHystrix
@EnableFeignClients
@SpringBootApplication
public class HystrixConsumerApplication6830 {

    public static void main(String[] args) {
        SpringApplication.run(HystrixConsumerApplication6830.class, args);
    }
}
