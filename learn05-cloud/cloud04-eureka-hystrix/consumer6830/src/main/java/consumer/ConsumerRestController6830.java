package consumer;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author benshaoye
 */
@Slf4j
@RequestMapping("rest")
@RestController
public class ConsumerRestController6830 {

    @Autowired
    private RestTemplate restTemplate;

    private static String targetNode = "http://hystris-provider/";

    private Map getTargetMap(String suffix) {
        return restTemplate.getForObject(targetNode + suffix, Map.class);
    }

    @GetMapping("getMaleInfo")
    public Map getMaleInfo() {
        log.info("===================================================================");
        log.info("rest 收到请求: {}#{}", getClass().getName(), "getMaleInfo");
        Map result = getTargetMap("getMaleInfo");
        log.info("rest 收到结果: {}", result);
        return result;
    }

    @GetMapping("getFemaleInfo")
    public Map getFemaleInfo() {
        log.info("===================================================================");
        log.info("rest 收到请求: {}#{}", getClass().getName(), "getFemaleInfo");
        Map result = getTargetMap("getFemaleInfo");
        log.info("rest 收到结果: {}", result);
        return result;
    }

    @GetMapping("getThrowInfo")
    @HystrixCommand(fallbackMethod = "getThrowInfoFallback")
    public Map getThrowInfo() {
        log.info("===================================================================");
        log.info("rest 收到请求: {}#{}", getClass().getName(), "getThrowInfo");
        Map result = getTargetMap("getThrowInfo");
        log.info("rest 收到结果: {}", result);
        return result;
    }

    public Map getThrowInfoFallback() {
        return new HashMap(2) {{
            put("info", "远程服务异常熔断了");
        }};
    }
}
