package consumer;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author benshaoye
 */
@Slf4j
@RequestMapping("feign")
@RestController
public class ConsumerFeignController6830 {

    @Autowired
    private GetInfoService getInfoService;

    @GetMapping("getMaleInfo")
    @HystrixCommand(fallbackMethod = "defaultFallback")
    public Map getMaleInfo() {
        log.info("===================================================================");
        log.info("feign 收到请求: {}#{}", getClass().getName(), "getMaleInfo");
        Map result = getInfoService.getMaleInfo();
        log.info("feign 收到结果: {}", result);
        return result;
    }

    @GetMapping("getFemaleInfo")
    public Map getFemaleInfo() {
        log.info("===================================================================");
        log.info("feign 收到请求: {}#{}", getClass().getName(), "getMaleInfo");
        Map result = getInfoService.getFemaleInfo();
        log.info("feign 收到结果: {}", result);
        return result;
    }

    @GetMapping("getThrowInfo")
    public Map getThrowInfo() {
        log.info("===================================================================");
        log.info("feign 收到请求: {}#{}", getClass().getName(), "getMaleInfo");
        Map result = getInfoService.getThrowInfo();
        log.info("feign 收到结果: {}", result);
        return result;
    }

    public Map defaultFallback(Throwable throwable) {
        log.info("远程服务异常;", throwable);
        return new HashMap(2){{
            put("info", "Feign: 远程服务异常");
        }};
    }
}
