package consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

/**
 * @author benshaoye
 */
@Slf4j
@RequestMapping("throw")
@RestController
public class ConsumerThrowController6830 {

    @Autowired
    private RestTemplate restTemplate;

    private static String targetNode = "http://hystris-provider/";

    private Map getTargetMap(String suffix) {
        return restTemplate.getForObject(targetNode + suffix, Map.class);
    }

    @GetMapping("getMaleInfo")
    public Map getMaleInfo() {
        return getTargetMap("getMaleInfo");
    }

    @GetMapping("getFemaleInfo")
    public Map getFemaleInfo() {
        return getTargetMap("getFemaleInfo");
    }

    @GetMapping("getThrowInfo")
    public Map getThrowInfo() {
        return getTargetMap("getThrowInfo");
    }

}
