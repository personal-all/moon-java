package consumer;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

/**
 * @author benshaoye
 */
@FeignClient("hystris-provider")
public interface GetInfoService {

    @GetMapping("getMaleInfo")
    Map getMaleInfo();

    @GetMapping("getFemaleInfo")
    Map getFemaleInfo();

    @GetMapping("getThrowInfo")
    Map getThrowInfo();
}
