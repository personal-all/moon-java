package consumer;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author benshaoye
 */
@Component("getInfoServiceFallback")
public class GetInfoServiceFallback implements GetInfoService {

    public GetInfoServiceFallback() {
    }

    @Override
    public Map getInfo() {
        System.out.println("熔断");
        return new HashMap(2){{
            put("status", "熔断");
        }};
    }
}
