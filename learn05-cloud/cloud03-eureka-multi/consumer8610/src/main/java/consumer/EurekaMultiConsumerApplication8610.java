package consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author benshaoye
 */
@Slf4j
@EnableFeignClients
@SpringBootApplication
public class EurekaMultiConsumerApplication8610 {

    public static void main(String[] args) {
        SpringApplication.run(EurekaMultiConsumerApplication8610.class, args);
    }
}
