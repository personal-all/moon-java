package consumer;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

/**
 * @author benshaoye
 */
@FeignClient(value = "learn05-cloud.cloud03-eureka-multi.provider", fallback = GetInfoServiceFallback.class)
public interface GetInfoService {

    @GetMapping("/getInfo")
    Map getInfo();
}
