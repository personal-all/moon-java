package consumer;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author benshaoye
 */
@Slf4j
@RestController
public class ConsumerController8610 {

    @Resource
    // @Qualifier("getInfoServiceFallback")
    private GetInfoService getInfoService;
    @Resource
    private RestTemplate restTemplate;

    @GetMapping("getInfo")
    public Map getInfo() {
        log.info("收到请求 Ctrl: {}#getInfo()", getClass());
        return getInfoService.getInfo();
    }

    @GetMapping("getRestInfo")
    public Map getRestInfo() {
        log.info("收到请求 Ctrl: {}#getRestInfo()", getClass());
        String appName = "learn05-cloud.cloud03-eureka-multi.provider";
        // appName = "provider8310";
        Map result = restTemplate.getForObject(
            "http://" + appName + ":8310/getInfo", Map.class);
        log.info(String.valueOf(result));
        return result;
    }
}
