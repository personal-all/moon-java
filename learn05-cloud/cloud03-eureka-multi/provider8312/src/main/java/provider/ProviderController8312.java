package provider;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author benshaoye
 */
@Slf4j
@RestController
public class ProviderController8312 {

    @Value("${spring.application.name}")
    private String applicationName;

    @Value("${server.port}")
    private String serverPort;

    @GetMapping("getInfo")
    public Map getInfo() {
        log.info("Controller: {}, app.name: {}, port: {}", getClass().getSimpleName(), applicationName, serverPort);
        return new HashMap(4) {{
            put("name", "1234567890");
            put("age", 12);
            put("sex", "男");
            put("address", "北京市海淀区");
            put("class", getClass().getName());
        }};
    }
}
