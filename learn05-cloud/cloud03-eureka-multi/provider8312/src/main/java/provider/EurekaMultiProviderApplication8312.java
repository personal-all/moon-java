package provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author benshaoye
 */
@SpringBootApplication
public class EurekaMultiProviderApplication8312 {

    public static void main(String[] args) {
        SpringApplication.run(EurekaMultiProviderApplication8312.class, args);
    }
}
