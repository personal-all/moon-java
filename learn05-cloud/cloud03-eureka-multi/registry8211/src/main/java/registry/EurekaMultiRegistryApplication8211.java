package registry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author benshaoye
 */
@EnableEurekaServer
@SpringBootApplication
public class EurekaMultiRegistryApplication8211 {

    public static void main(String[] args) {
        SpringApplication.run(EurekaMultiRegistryApplication8211.class, args);
    }
}

