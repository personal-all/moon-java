package eureka;

import com.moon.core.util.IteratorUtil;
import com.moon.more.excel.ExcelUtil;
import org.apache.commons.lang.StringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * @author benshaoye
 */
class EurekaApplicationTestTest {

    @Test
    void testMain() throws Exception {
    }

    @ParameterizedTest
    @ValueSource(strings = "D:/【同在文化】2019年5月1日-2020年4月30日公司流水-20200518.txt")
    void testExportText(String filepath) throws Exception {
        ExcelUtil.xlsx().sheet(sheetFactory -> {
            IteratorUtil.forEachLines(filepath, line -> {
                String[] values = StringUtils.split(line, '|');
                sheetFactory.row(rowFactory -> {
                    for (String value : values) {
                        rowFactory.next(value);
                    }
                });
            });
        }).write2Filepath("D:/【同在文化】2019年5月1日-2020年4月30日公司流水-20200518.xlsx");
    }
}