package consumer;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author benshaoye
 */
@Component
public class GetInfoServiceImpl implements GetInfoService {

    @Override
    public Map getMaleInfo() {
        return new HashMap(4){{
            put("info", "不知道什么原因，服务熔断了 getMaleInfo");
        }};
    }

    @Override
    public Map getThrowInfo() {
        return new HashMap(4){{
            put("info", "不知道什么原因，服务熔断了 getThrowInfo");
        }};

    }
}
