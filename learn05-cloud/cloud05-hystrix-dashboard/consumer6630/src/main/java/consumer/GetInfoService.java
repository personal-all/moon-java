package consumer;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

/**
 * @author benshaoye
 */
@FeignClient(value = "learn05-cloud.cloud05-hystrix-dashboard.provider", fallback = GetInfoServiceImpl.class)
public interface GetInfoService {

    @GetMapping("getMaleInfo")
    Map getMaleInfo();

    @GetMapping("getThrowInfo")
    Map getThrowInfo();
}
