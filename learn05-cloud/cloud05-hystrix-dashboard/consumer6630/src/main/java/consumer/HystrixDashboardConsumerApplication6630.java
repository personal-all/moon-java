package consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author benshaoye
 */
@EnableHystrix
@EnableFeignClients
@SpringBootApplication
public class HystrixDashboardConsumerApplication6630 {

    public static void main(String[] args) {
        SpringApplication.run(HystrixDashboardConsumerApplication6630.class, args);
    }
}
