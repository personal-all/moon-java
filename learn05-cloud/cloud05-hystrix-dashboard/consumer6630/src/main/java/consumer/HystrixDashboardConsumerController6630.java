package consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author benshaoye
 */
@RestController
public class HystrixDashboardConsumerController6630 {

    @Resource
    private GetInfoService getInfoService;

    @GetMapping("getMaleInfo")
    public Map getMaleInfo(){
        return getInfoService.getMaleInfo();
    }

    @GetMapping("getThrowInfo")
    public Map getThrowInfo(){
        return getInfoService.getThrowInfo();
    }
}
