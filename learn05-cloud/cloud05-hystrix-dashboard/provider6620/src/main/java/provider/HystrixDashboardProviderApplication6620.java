package provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * @author benshaoye
 */
@EnableHystrix
@EnableHystrixDashboard
@SpringBootApplication
public class HystrixDashboardProviderApplication6620 {

    public static void main(String[] args) {
        SpringApplication.run(HystrixDashboardProviderApplication6620.class, args);
    }
}
