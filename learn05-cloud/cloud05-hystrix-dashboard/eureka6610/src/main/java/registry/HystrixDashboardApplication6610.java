package registry;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author benshaoye
 */
@Slf4j
@EnableEurekaServer
@SpringBootApplication
public class HystrixDashboardApplication6610 {

    public static void main(String[] args) {
        SpringApplication.run(HystrixDashboardApplication6610.class, args);
    }
}
