package provider;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author benshaoye
 */
@Slf4j
@RestController
public class HystrixDashboardProviderController6621 {

    @GetMapping("getMaleInfo")
    public Map getMaleInfo() {
        return new HashMap(8) {{
            put("name", "张大炮");
            put("age", 24);
            put("sex", "男");
            put("address", "广州白云区");
        }};
    }

    @GetMapping("getThrowInfo")
    // @HystrixCommand(fallbackMethod = "getThrowInfoOnFallback")
    public Map getThrowInfo() {
        throw new IllegalStateException();
    }


    public Map getThrowInfoOnFallback() {
        String classname = getClass().getSimpleName();
        return new HashMap(2) {{
            put("info", classname + " > 未知原因：服务被熔断了");
        }};
    }
}
