package eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author benshaoye
 */
@EnableEurekaServer// 开启 eureka 注册中心
@SpringBootApplication
public class EurekaApplication8024 {

    public static void main(String[] args) {
        SpringApplication.run(EurekaApplication8024.class, args);
    }
}
