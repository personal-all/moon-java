package provider;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author benshaoye
 */
@Slf4j
@RestController
public class ProviderController {

    @GetMapping("getInfo")
    public Map getInfo(HttpServletRequest request) {
        log.info("{} 接收到请求： {}", getClass().getSimpleName(), request.getRequestURI());
        return new HashMap(4) {{
            put("name", "张三");
            put("age", 24);
            put("sex", "男");
        }};
    }
}
