package provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author benshaoye
 */
// @EnableEurekaClient
@SpringBootApplication
public class ProviderApplication8026 {

    public static void main(String[] args) {
        SpringApplication.run(ProviderApplication8026.class, args);
    }
}
