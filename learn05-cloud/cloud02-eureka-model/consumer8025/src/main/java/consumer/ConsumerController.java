package consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author benshaoye
 */
@Slf4j
@RestController
public class ConsumerController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("getInfo")
    public Map getInfo(HttpServletRequest request) {
        String targetNodeName = "learn05-cloud.cloud02-eureka-model.provider8026";
        log.info("{} 接收到请求： {}", getClass().getSimpleName(), request.getRequestURI());
        String url = "http://" + targetNodeName + ":8026/getInfo";
        return restTemplate.getForObject(url, Map.class);
    }
}
