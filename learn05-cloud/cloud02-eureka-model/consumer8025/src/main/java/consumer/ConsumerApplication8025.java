package consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author benshaoye
 */
// eureka 客户端，开启后会自动注册进 eureka 服务中
// Spring E 版后默认只要有 eureka-client 依赖，不用添加这个注解也会自动检测和执行注册
// 相应的还有 EnableDiscoveryClient: 可以向非 eureka 注册
// @EnableEurekaClient: 只能向 eureka 注册
@SpringBootApplication
public class ConsumerApplication8025 {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication8025.class, args);
    }
}
