package consumer;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author benshaoye
 */
@Configuration
public class RestTemplateConfig {

    /**
     * 注意，这里创建{@link RestTemplate}的方式，要通过{@link RestTemplateBuilder}创建
     * <p>
     * 我在两台电脑，分别用{@code new RestTemplate()}和{@link RestTemplateBuilder#build()}创建
     * <p>
     * 这台电脑{@code new RestTemplate()}创建的总是报超时错误(服务注册什么的都没问题)
     *
     * @param builder
     *
     * @return
     */
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }
}
